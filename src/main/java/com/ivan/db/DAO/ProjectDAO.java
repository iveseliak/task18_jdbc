package com.ivan.db.DAO;


import com.ivan.db.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
