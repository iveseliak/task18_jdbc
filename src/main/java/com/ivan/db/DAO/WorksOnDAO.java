package com.ivan.db.DAO;


import com.ivan.db.model.PK_WorksOn;
import com.ivan.db.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
